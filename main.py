import json

import feedparser

JSONP_TEMPLATE = "jornal10_render({}, {});"

news = {
    'BR': [
        'http://oglobo.globo.com/rss.xml?secao=ece_frontpage',
        'http://feeds.feedburner.com/pragmatismopolitico',
        'https://services.nexojornal.com.br/content/mini-home/latest-news.xml',
        'http://odia.ig.com.br/rss.xml',
        'http://extra.globo.com/rss.xml',
        'http://ep01.epimg.net/rss/brasil/portada.xml',
        'http://www.cartacapital.com.br/atom.xml',
        'http://www.vice.com/pt_br/rss',
        'http://www.huffingtonpost.com/feeds/verticals/brazil/news.xml',
        'http://zh.clicrbs.com.br/rs/ultimas-noticias-rss/',
        'http://www.gazetaonline.com.br/_conteudo/noticias/rss.xml', # es
        'http://www.tribunaonline.com.br/feed/',
        'http://www.correiobraziliense.com.br/rss/noticia/brasil/rss.xml',
        'http://www.valor.com.br/brasil/rss',
        'http://www.opopular.com.br/cmlink/o-popular-%C3%BAltimas-1.272904',
        'http://new.d24am.com/rss',

    ],
    'US': [
        'http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml',
    ]
}

# http://www.correiobraziliense.com.br/rss/
# http://www.valor.com.br/pagina/rss

def get_news(url):
    try:
        d = feedparser.parse(url)
        title = d['feed']['title']
        entries = []

        for entry in d.entries:
            entries.append(dict(title=entry['title'],
                                link=entry['link'],
                                desc=entry['description']))

        return title, entries
    except:
        return None, []

objs = json.dumps([get_news(url) for url in news['BR'] if get_news(url)[0] != None], indent=4)

print(JSONP_TEMPLATE.format(objs, "main"))
